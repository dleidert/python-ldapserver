Python LDAP Server Library
==========================

Python library that implements the socketserver.BaseRequestHandler interface
for LDAP. It provides stubs for all operations (BIND, SEARCH, ...). Overwrite
the stubs you need to provide actual functionality.

See the [documentation](https://uffd.pages.git.cccv.de/python-ldapserver) for
further details.
