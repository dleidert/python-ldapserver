import logging
import socketserver
import pwd
import grp

import ldapserver

logging.basicConfig(level=logging.INFO)

class RequestHandler(ldapserver.LDAPRequestHandler):
	subschema = ldapserver.SubschemaSubentry(ldapserver.schema.RFC2307BIS_SCHEMA, 'cn=Subschema')

	def do_search(self, basedn, scope, filterobj):
		yield from super().do_search(basedn, scope, filterobj)
		yield self.subschema.ObjectEntry('dc=example,dc=com', **{
			'objectClass': ['top', 'dcObject', 'organization'],
			'structuralObjectClass': ['organization'],
		})
		user_gids = {}
		for user in pwd.getpwall():
			user_gids[user.pw_gid] = user_gids.get(user.pw_gid, set()) | {user.pw_name}
			yield self.subschema.ObjectEntry(self.subschema.DN('ou=users,dc=example,dc=com', uid=user.pw_name), **{
				'objectClass': ['top', 'organizationalperson', 'person', 'posixaccount'],
				'structuralObjectClass': ['organizationalperson'],
				'uid': [user.pw_name],
				'uidNumber': [user.pw_uid],
				'gidNumber': [user.pw_gid],
				'cn': [user.pw_gecos],
			})
		for group in grp.getgrall():
			members = set(group.gr_mem) | user_gids.get(group.gr_gid, set())
			yield self.subschema.ObjectEntry(self.subschema.DN('ou=groups,dc=example,dc=com', cn=group.gr_name), **{
				'objectClass': ['top', 'groupOfUniqueNames', 'posixGroup'],
				'structuralObjectClass': ['groupOfUniqueNames'],
				'cn': [group.gr_name],
				'gidNumber': [group.gr_gid],
				'uniqueMember': [self.subschema.DN('ou=user,dc=example,dc=com', uid=name) for name in members],
			})

if __name__ == '__main__':
	socketserver.ThreadingTCPServer(('127.0.0.1', 3890), RequestHandler).serve_forever()
