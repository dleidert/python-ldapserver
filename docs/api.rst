API Reference
=============

Request Handler
---------------

.. autoclass:: ldapserver.LDAPRequestHandler
	:members:
	:exclude-members: do_bind_simple, do_bind_sasl

	.. autoattribute:: logger

Distinguished Names
-------------------

.. autoclass:: ldapserver.DN
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.RDN
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.RDNAssertion
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.DNWithUID
	:members:
	:special-members: __str__

Entries
-------

An LDAP server provides access to a tree of directory entries (the DIT).
There are different kinds of entries: Object entries holding actual data objects (see :class:`ObjectEntry`), alias entries providing alternative naming for other entries (not supported) and subentries holding administrative/operational information (see :class:`SubschemaSubentry` and :class:`RootDSE`).

.. autoclass:: ldapserver.AttributeDict
	:members:

.. autoclass:: ldapserver.Entry
	:members:

.. autoclass:: ldapserver.RootDSE
	:members:

.. autoclass:: ldapserver.SubschemaSubentry
	:members:

.. py:data:: ldapserver.WILDCARD

	Special wildcard singleton for :class:`EntryTemplate`

.. autoclass:: ldapserver.EntryTemplate
	:members:

Schema
------

The :class:`schema.Schema` class and the schema-bound classes like :class:`schema.Syntax` provide a high-level interface to the low-level schema definition objects.

Objects of the schema-bound classes reference other objects within their schema directly.
They are automatically instanciated when a :any:`schema.Schema` object is created based on the passed definition objects.

.. autoclass:: ldapserver.schema.Schema
	:members:

.. autoclass:: ldapserver.schema.ObjectClass
	:members:

.. autoclass:: ldapserver.schema.AttributeType
	:members:

.. autoclass:: ldapserver.schema.EqualityMatchingRule
	:members:
	:inherited-members:

.. autoclass:: ldapserver.schema.OrderingMatchingRule
	:members:
	:inherited-members:

.. autoclass:: ldapserver.schema.SubstrMatchingRule
	:members:
	:inherited-members:

.. autoclass:: ldapserver.schema.Syntax
	:members:

Low-level Schema Definitions
----------------------------

:class:`schema.AttributeTypeDefinition` and :class:`schema.ObjectClassDefinition` objects are nothing more than parsed representations of attribute type and object class definition strings found in standard documents such as RFC4519.
:class:`schema.MatchingRuleDefinition` and :class:`schema.SyntaxDefinition` also provide methods for encoding/decoding or matching.

Instance attributes of definition objects are named after the definition string keywords converted to snake-case (e.g. `NAME` becomes `name`, `NO-USER-MODIFICATION` becomes `no_user_modification`).

.. autoclass:: ldapserver.schema.SyntaxDefinition
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.schema.MatchingRuleDefinition
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.schema.MatchingRuleUseDefinition
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.schema.AttributeTypeUsage
	:members:

.. autoclass:: ldapserver.schema.AttributeTypeDefinition
	:members:
	:special-members: __str__

.. autoclass:: ldapserver.schema.ObjectClassKind
	:members:

.. autoclass:: ldapserver.schema.ObjectClassDefinition
	:members:
	:special-members: __str__

Built-in Schemas
----------------

.. autodata:: ldapserver.schema.RFC4512_SCHEMA
	:no-value:
.. autodata:: ldapserver.schema.CORE_SCHEMA
	:annotation: = RFC4512_SCHEMA

.. autodata:: ldapserver.schema.RFC4519_SCHEMA
	:no-value:

.. autodata:: ldapserver.schema.RFC4523_SCHEMA
  :no-value:

.. autodata:: ldapserver.schema.RFC4524_SCHEMA
	:no-value:
.. autodata:: ldapserver.schema.COSINE_SCHEMA
	:annotation: = RFC4524_SCHEMA

.. autodata:: ldapserver.schema.RFC3112_SCHEMA
	:no-value:

.. autodata:: ldapserver.schema.RFC2079_SCHEMA
	:no-value:

.. autodata:: ldapserver.schema.RFC2798_SCHEMA
	:no-value:
.. autodata:: ldapserver.schema.INETORG_SCHMEA
	:annotation: = RFC2798_SCHEMA

.. autodata:: ldapserver.schema.RFC2307BIS_SCHEMA
	:no-value:

LDAP Protocol
-------------

.. autoclass:: ldapserver.ldap.SearchScope
	:members:
	:undoc-members:

.. autoclass:: ldapserver.ldap.Filter
	:members:
.. autoclass:: ldapserver.ldap.FilterAnd
	:members:
.. autoclass:: ldapserver.ldap.FilterOr
	:members:
.. autoclass:: ldapserver.ldap.FilterNot
	:members:
.. autoclass:: ldapserver.ldap.FilterPresent
	:members:
.. autoclass:: ldapserver.ldap.FilterEqual
	:members:
.. autoclass:: ldapserver.ldap.FilterApproxMatch
	:members:
.. autoclass:: ldapserver.ldap.FilterGreaterOrEqual
	:members:
.. autoclass:: ldapserver.ldap.FilterLessOrEqual
	:members:
.. autoclass:: ldapserver.ldap.FilterSubstrings
	:members:
.. autoclass:: ldapserver.ldap.FilterExtensibleMatch
	:members:

.. autoclass:: ldapserver.ldap.SearchResultEntry
	:members:
.. autoclass:: ldapserver.ldap.PartialAttribute
	:members:

LDAP Errors
-----------

LDAP response messages carry a result code and an optional diagnostic message.
The subclasses of :any:`ldapserver.exceptions.LDAPError` represent the
possible (non-success) result codes.

Raising an :any:`ldapserver.exceptions.LDAPError` instance in a handler method
of :any:`ldapserver.LDAPRequestHandler` aborts the request processing and sends
an appropriate response message with the corresponding result code and (if any)
the diagnostic message.

.. autoexception:: ldapserver.exceptions.LDAPError
.. autoexception:: ldapserver.exceptions.LDAPOperationsError
.. autoexception:: ldapserver.exceptions.LDAPProtocolError
.. autoexception:: ldapserver.exceptions.LDAPTimeLimitExceeded
.. autoexception:: ldapserver.exceptions.LDAPSizeLimitExceeded
.. autoexception:: ldapserver.exceptions.LDAPAuthMethodNotSupported
.. autoexception:: ldapserver.exceptions.LDAPStrongerAuthRequired
.. autoexception:: ldapserver.exceptions.LDAPAdminLimitExceeded
.. autoexception:: ldapserver.exceptions.LDAPUnavailableCriticalExtension
.. autoexception:: ldapserver.exceptions.LDAPConfidentialityRequired
.. autoexception:: ldapserver.exceptions.LDAPNoSuchAttribute
.. autoexception:: ldapserver.exceptions.LDAPUndefinedAttributeType
.. autoexception:: ldapserver.exceptions.LDAPInappropriateMatching
.. autoexception:: ldapserver.exceptions.LDAPConstraintViolation
.. autoexception:: ldapserver.exceptions.LDAPAttributeOrValueExists
.. autoexception:: ldapserver.exceptions.LDAPInvalidAttributeSyntax
.. autoexception:: ldapserver.exceptions.LDAPNoSuchObject
.. autoexception:: ldapserver.exceptions.LDAPAliasProblem
.. autoexception:: ldapserver.exceptions.LDAPInvalidDNSyntax
.. autoexception:: ldapserver.exceptions.LDAPAliasDereferencingProblem
.. autoexception:: ldapserver.exceptions.LDAPInappropriateAuthentication
.. autoexception:: ldapserver.exceptions.LDAPInvalidCredentials
.. autoexception:: ldapserver.exceptions.LDAPInsufficientAccessRights
.. autoexception:: ldapserver.exceptions.LDAPBusy
.. autoexception:: ldapserver.exceptions.LDAPUnavailable
.. autoexception:: ldapserver.exceptions.LDAPUnwillingToPerform
.. autoexception:: ldapserver.exceptions.LDAPLoopDetect
.. autoexception:: ldapserver.exceptions.LDAPNamingViolation
.. autoexception:: ldapserver.exceptions.LDAPObjectClassViolation
.. autoexception:: ldapserver.exceptions.LDAPNotAllowedOnNonLeaf
.. autoexception:: ldapserver.exceptions.LDAPNotAllowedOnRDN
.. autoexception:: ldapserver.exceptions.LDAPEntryAlreadyExists
.. autoexception:: ldapserver.exceptions.LDAPObjectClassModsProhibited
.. autoexception:: ldapserver.exceptions.LDAPAffectsMultipleDSAs
.. autoexception:: ldapserver.exceptions.LDAPOther

.. _RFC 4513: https://tools.ietf.org/html/rfc4513
.. _RFC 4513 5.1.1.: https://tools.ietf.org/html/rfc4513#section-5.1.1
.. _RFC 4513 5.1.2.: https://tools.ietf.org/html/rfc4513#section-5.1.2
.. _RFC 4513 5.1.3.: https://tools.ietf.org/html/rfc4513#section-5.1.3
