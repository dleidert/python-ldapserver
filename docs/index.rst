Welcome to python-ldapserver's documentation
============================================

python-ldapserver is a self-contained pure Python 3 library for implementing special-purpose LDAPv3 servers.
It aims for full RFC4510 compliance.

A typical use case is glue code for integrating LDAP clients (e.g. web applications that use LDAP for authentication and user synchonization) with non-LDAP data sources (e.g. HTTP APIs).

The project is in an early stage of development.
You can use it to write a fully-functional and stable read-only LDAP server.
However, expect significant API changes in future releases.
It is versioned according to `SemVer <http://semver.org/>`_.
Although it is pre-v1, patch releases are only published for backwards compatible bug fixes.

The source code is available under the terms of the `GNU Affero General Public License Version 3 <https://www.gnu.org/licenses/agpl-3.0.html>`_.
It is hosted at `<https://git.cccv.de/uffd/python-ldapserver>`_.
Releases are also available on `PyPI <https://pypi.org/project/ldapserver/>`_.

API Reference
-------------

.. toctree::
  :maxdepth: 2

  api

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
